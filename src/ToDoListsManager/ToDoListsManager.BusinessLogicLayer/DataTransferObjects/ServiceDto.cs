﻿namespace ToDoListsManager.BusinessLogicLayer.DataTransferObjects
{
    public class ServiceDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
