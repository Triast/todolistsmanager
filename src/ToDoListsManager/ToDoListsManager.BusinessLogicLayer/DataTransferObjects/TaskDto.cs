﻿namespace ToDoListsManager.BusinessLogicLibrary.DataTransferObjects
{
    public class TaskDto
    {
        public int Id { get; set; }
        public int LocalId { get; set; }
        public string ServiceId { get; set; }
        public string Text { get; set; }
        public bool IsCompleted { get; set; }
    }
}
