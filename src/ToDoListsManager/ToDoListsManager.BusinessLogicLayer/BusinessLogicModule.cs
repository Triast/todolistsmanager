﻿using Autofac;
using ToDoListsManager.BusinessLogicLayer.Services;
using ToDoListsManager.BusinessLogicLibrary.Services;
using ToDoListsManager.DataAccessLayer;

namespace ToDoListsManager.BusinessLogicLibrary
{
    public class BusinessLogicModule : Module
    {
        public string ConnectionString { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule(new DataAccessModule { ConnectionString = ConnectionString });

            builder.RegisterType<TaskService>().As<ITaskService>();
            builder.RegisterType<TodoListService>().As<ITodoListService>();
        }
    }
}
