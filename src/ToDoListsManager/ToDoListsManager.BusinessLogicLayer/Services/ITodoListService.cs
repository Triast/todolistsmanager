﻿using System.Collections.Generic;
using ToDoListsManager.BusinessLogicLayer.DataTransferObjects;

namespace ToDoListsManager.BusinessLogicLayer.Services
{
    public interface ITodoListService
    {
        ServiceDto Get(string id);
        IEnumerable<ServiceDto> GetAll();

        string Add(ServiceDto service);
        void Remove(string serviceId);
        void Update(ServiceDto service);
    }
}
