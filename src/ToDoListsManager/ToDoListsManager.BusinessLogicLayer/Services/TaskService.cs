﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;
using ToDoListsManager.DataAccessLayer.Models;
using ToDoListsManager.DataAccessLayer.Repositories;

namespace ToDoListsManager.BusinessLogicLibrary.Services
{
    internal class TaskService : ITaskService
    {
        private readonly ITaskRepository _repo;

        public TaskService(ITaskRepository repo)
        {
            _repo = repo;
        }

        public int Add(TaskDto task)
        {
            if (String.IsNullOrWhiteSpace(task.Text))
            {
                throw new ArgumentException("Task's text must contains letters.");
            }
            //if (task.IsCompleted)
            //{
            //    throw new ArgumentException("Task must be incompleted when adding new task");
            //}

            return _repo.Add(TaskFromDto(task));
        }

        public void Complete(TaskDto task)
        {
            var dbTask = _repo.Get(task.LocalId, task.ServiceId);

            if (dbTask == null)
            {
                throw new ArgumentException("Attempt to complete nonexisting task.");
            }

            dbTask.IsCompleted = true;
            _repo.Update(dbTask);
        }

        public TaskDto Get(int id) => DtoFromTask(_repo.Get(id));
        public TaskDto Get(int localId, string serviceId)
        {
            return DtoFromTask(_repo.Get(localId, serviceId));
        }

        public IEnumerable<TaskDto> GetAll() => _repo.GetAll().Select(DtoFromTask);

        public void Remove(TaskDto task)
        {
            var dbTask = _repo.Get(task.LocalId, task.ServiceId);

            if (dbTask == null)
            {
                throw new ArgumentException("Attempt to remove nonexisting task.");
            }

            _repo.Remove(dbTask);
        }

        private Task TaskFromDto(TaskDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Task
            {
                Id = dto.Id,
                LocalId = dto.LocalId,
                ServiceId = dto.ServiceId,
                Text = dto.Text,
                IsCompleted = dto.IsCompleted
            };
        }

        private TaskDto DtoFromTask(Task task)
        {
            if (task == null)
            {
                return null;
            }

            return new TaskDto
            {
                Id = task.Id,
                LocalId = task.LocalId,
                ServiceId = task.ServiceId,
                Text = task.Text,
                IsCompleted = task.IsCompleted
            };
        }
    }
}
