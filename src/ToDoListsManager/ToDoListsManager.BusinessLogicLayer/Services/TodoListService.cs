﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListsManager.BusinessLogicLayer.DataTransferObjects;
using ToDoListsManager.DataAccessLayer.Models;
using ToDoListsManager.DataAccessLayer.Repositories;

namespace ToDoListsManager.BusinessLogicLayer.Services
{
    internal class TodoListService : ITodoListService
    {
        private readonly IServiceRepository _repo;

        public TodoListService(IServiceRepository repo)
        {
            _repo = repo;
        }

        public string Add(ServiceDto service)
        {
            if (String.IsNullOrWhiteSpace(service.Name))
            {
                throw new ArgumentException("Service's name must contains letters.");
            }

            return _repo.Add(ServiceFromDto(service));
        }

        public void Update(ServiceDto service)
        {
            var dbTask = _repo.Get(service.Id);

            if (dbTask == null)
            {
                throw new ArgumentException("Attempt to update nonexisting service.");
            }

            dbTask.Name = service.Name;
            _repo.Update(dbTask);
        }

        public ServiceDto Get(string id) => DtoFromService(_repo.Get(id));

        public IEnumerable<ServiceDto> GetAll() => _repo.GetAll().Select(DtoFromService);

        public void Remove(string serviceId)
        {
            var dbTask = _repo.Get(serviceId);

            if (dbTask == null)
            {
                throw new ArgumentException("Attempt to remove nonexisting task.");
            }

            _repo.Remove(dbTask);
        }

        private Service ServiceFromDto(ServiceDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new Service
            {
                Id = dto.Id,
                Name = dto.Name
            };
        }

        private ServiceDto DtoFromService(Service service)
        {
            if (service == null)
            {
                return null;
            }

            return new ServiceDto
            {
                Id = service.Id,
                Name = service.Name
            };
        }
    }
}
