﻿using System.Collections.Generic;
using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;

namespace ToDoListsManager.BusinessLogicLibrary.Services
{
    public interface ITaskService
    {
        TaskDto Get(int id);
        TaskDto Get(int localId, string serviceId);
        IEnumerable<TaskDto> GetAll();

        int Add(TaskDto task);
        void Remove(TaskDto task);
        void Complete(TaskDto task);
    }
}
