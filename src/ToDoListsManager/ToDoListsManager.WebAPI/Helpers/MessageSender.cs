using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;
using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;
using ToDoListsManager.WebAPI.Models;

namespace ToDoListsManager.WebAPI.Helpers
{
    class MessageSender : IMessageSender
    {
        private IConfiguration _configuration;

        public MessageSender(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void Add(TaskDto task)
        {
            SendMessage("POST", task);
        }

        public void Update(TaskDto task)
        {
            SendMessage("PATCH", task);
        }

        public void Remove(TaskDto task)
        {
            SendMessage("DELETE", task);
        }

        private void SendMessage(string action, TaskDto task)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: task.ServiceId,
                                    durable: true,
                                    exclusive: false,
                                    autoDelete: false,
                                    arguments: null);

                var serviceTask = new LiteTaskDto
                {
                    Id = task.LocalId,
                    Text = task.Text,
                    IsCompleted = task.IsCompleted
                };

                var message = new TaskMessage
                {
                    ServiceId = task.ServiceId,
                    Action = action,
                    Data = JsonConvert.SerializeObject(serviceTask)
                };

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

                channel.BasicPublish(exchange: "",
                                    routingKey: task.ServiceId,
                                    basicProperties: null,
                                    body: body);
            }
        }
    }
}
