using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;

namespace ToDoListsManager.WebAPI.Helpers
{
    public interface IMessageSender
    {
        void Add(TaskDto task);
        void Update(TaskDto task);
        void Remove(TaskDto task);
    }
}
