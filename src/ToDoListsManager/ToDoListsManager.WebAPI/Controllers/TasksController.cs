﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;
using ToDoListsManager.BusinessLogicLibrary.Services;
using ToDoListsManager.WebAPI.Helpers;

namespace ToDoListsManager.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;
        private readonly IMessageSender _sender;

        /// <summary>
        /// Constructor which accepts task service.
        /// </summary>
        public TasksController(ITaskService taskService, IMessageSender sender)
        {
            _taskService = taskService;
            _sender = sender;
        }

        /// <summary>
        /// Returns all tasks.
        /// </summary>
        /// <response code="200">Returned all tasks as collection.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpGet]
        public ActionResult<List<TaskDto>> Get()
        {
            return Ok(_taskService.GetAll().ToList());
        }

        /// <summary>
        /// Returns task with specified id.
        /// </summary>
        /// <param name="id">
        /// Integer id of task.
        /// </param>
        /// <response code="200">Returned task with specified id.</response>
        /// <response code="400">There is no task with provided id.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpGet("{serviceId}/{id}")]
        public ActionResult<TaskDto> Get(string serviceId, int id)
        {
            var task = _taskService.Get(id);

            if (task == null)
            {
                return BadRequest();
            }

            return Ok(task);
        }

        /// <summary>
        /// Creates new task.
        /// </summary>
        /// <param name="task">
        /// Task to add.
        /// </param>
        /// <response code="200">New task is created and id is returned.</response>
        /// <response code="400">Some validation error occured.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpPost]
        public ActionResult<int> Post([FromBody] TaskDto task)
        {
            try
            {
                // var id = _taskService.Add(task);

                _sender.Add(task);

                return Ok();
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Sets competeness of task.
        /// </summary>
        /// <param name="id">
        /// Id of task to be completed.
        /// </param>
        /// <response code="200">Task is completed and returned.</response>
        /// <response code="400">Some validation errors occured.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpPatch("{serviceId}/{id}")]
        public ActionResult<TaskDto> Patch(string serviceId, int id)
        {
            try
            {
                _taskService.Complete(new TaskDto { LocalId = id, ServiceId = serviceId });

                var task = _taskService.Get(id, serviceId);

                _sender.Update(task);

                return Ok(task);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deletes task.
        /// </summary>
        /// <param name="id">
        /// Task to be deleted.
        /// </param>
        /// <response code="200">Taks successfully deleted.</response>
        /// <response code="400">Some validation errors occured.</response>
        /// <response code="500">Oops! Something went wrong.</response>
        [HttpDelete("{serviceId}/{id}")]
        public ActionResult Delete(string serviceId, int id)
        {
            try
            {
                var task = _taskService.Get(id, serviceId);

                _taskService.Remove(task);

                _sender.Remove(task);

                return Ok(task.Id);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
