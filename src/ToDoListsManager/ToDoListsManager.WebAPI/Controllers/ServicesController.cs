﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using ToDoListsManager.BusinessLogicLayer.DataTransferObjects;
using ToDoListsManager.BusinessLogicLayer.Services;
using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;
using ToDoListsManager.BusinessLogicLibrary.Services;
using ToDoListsManager.WebAPI.Models;

namespace ToDoListsManager.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        private readonly ITodoListService _todoListService;
        private readonly ITaskService _taskService;

        public ServicesController(ITodoListService todoListService, ITaskService taskService)
        {
            _todoListService = todoListService;
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<List<ServiceDto>> Get()
        {
            return _todoListService.GetAll().ToList();
        }

        [HttpPost]
        public ActionResult<int> Post([FromBody] RegisterModel model)
        {
            try
            {
                var id = _todoListService.Add(new ServiceDto { Id = model.Id, Name = model.Name });

                foreach (var task in model.Tasks)
                {
                    _taskService.Add(new TaskDto
                    {
                        LocalId = task.Id,
                        ServiceId = model.Id,
                        Text = task.Text,
                        IsCompleted = task.IsCompleted
                    });
                }

                return Ok(id);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
