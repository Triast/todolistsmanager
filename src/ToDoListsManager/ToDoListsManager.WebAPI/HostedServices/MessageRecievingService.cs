﻿using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;
using ToDoListsManager.BusinessLogicLibrary.Services;
using ToDoListsManager.WebAPI.Models;

namespace ToDoListsManager.WebAPI.HostedServices
{
    public class MessageRecievingService : IHostedService
    {
        private readonly ITaskService _taskService;

        private readonly IConnection connection;
        private readonly IModel channel;
        private readonly EventingBasicConsumer consumer;

        private const string _queueName = "manager_queue";

        public MessageRecievingService(ITaskService taskService)
        {
            _taskService = taskService;

            var factory = new ConnectionFactory() { HostName = "localhost" };

            connection = factory.CreateConnection();
            channel = connection.CreateModel();

            consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var message = JsonConvert.DeserializeObject<TaskMessage>(Encoding.UTF8.GetString(ea.Body));
                var task = JsonConvert.DeserializeObject<TaskDto>(message.Data);
                task.LocalId = task.Id;
                task.ServiceId = message.ServiceId;
                task.Id = 0;

                switch (message.Action)
                {
                    case "POST":
                        _taskService.Add(task);
                        break;
                    case "PATCH":
                        _taskService.Complete(task);
                        break;
                    case "DELETE":
                        _taskService.Remove(task);
                        break;
                }
            };
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            channel.QueueDeclare(queue: _queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

            channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
            channel.BasicConsume(queue: _queueName,
                                autoAck: true,
                                consumer: consumer);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            connection.Close();

            return Task.CompletedTask;
        }
    }
}
