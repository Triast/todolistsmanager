namespace ToDoListsManager.WebAPI.Models
{
    internal class TaskViewModel
    {
        public int Id { get; set; }
        public string ServiceId { get; set; }
        public string Text { get; set; }
        public bool IsCompleted { get; set; }
    }
}