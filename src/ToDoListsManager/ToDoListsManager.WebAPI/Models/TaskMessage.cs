namespace ToDoListsManager.WebAPI.Models
{
    internal class TaskMessage
    {
        public string ServiceId { get; set; }
        public string Action { get; set; }
        public string Data { get; set; }
    }
}