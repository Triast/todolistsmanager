﻿using System.Collections.Generic;
using ToDoListsManager.BusinessLogicLibrary.DataTransferObjects;

namespace ToDoListsManager.WebAPI.Models
{
    public class RegisterModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<TaskDto> Tasks { get; set; }
    }
}
