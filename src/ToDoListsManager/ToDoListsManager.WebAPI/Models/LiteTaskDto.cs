namespace ToDoListsManager.WebAPI.Models
{
    public class LiteTaskDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsCompleted { get; set; }
    }
}