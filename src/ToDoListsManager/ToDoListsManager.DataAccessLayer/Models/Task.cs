﻿namespace ToDoListsManager.DataAccessLayer.Models
{
    public class Task
    {
        public int Id { get; set; }
        public int LocalId { get; set; }
        public string ServiceId { get; set; }
        public string Text { get; set; }
        public bool IsCompleted { get; set; }
    }
}
