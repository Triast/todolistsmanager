﻿using Autofac;
using ToDoListsManager.DataAccessLayer.Contexts;
using ToDoListsManager.DataAccessLayer.Repositories;

namespace ToDoListsManager.DataAccessLayer
{
    public class DataAccessModule : Module
    {
        public string ConnectionString { get; set; }

        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ToDoListContext>()
                .WithParameter(new TypedParameter(typeof(string), ConnectionString));

            builder.RegisterType<TaskRepository>().As<ITaskRepository>();
            builder.RegisterType<ServiceRepository>().As<IServiceRepository>();
        }
    }
}
