﻿using System.Collections.Generic;
using System.Linq;
using ToDoListsManager.DataAccessLayer.Models;

namespace ToDoListsManager.DataAccessLayer.Repositories
{
    internal class ServiceRepository : IServiceRepository
    {
        private readonly Contexts.ToDoListContext _ctx;

        public ServiceRepository(Contexts.ToDoListContext ctx)
        {
            _ctx = ctx;
        }

        public string Add(Service entity)
        {
            var entry = _ctx.Services.Add(entity);
            _ctx.SaveChanges();
            
            return entity.Id;
        }

        public Service Get(string id) => _ctx.Services.Find(id);

        public IEnumerable<Service> GetAll() => _ctx.Services.AsEnumerable();

        public void Remove(Service entity)
        {
            var service = _ctx.Services.Find(entity.Id);
            _ctx.Services.Remove(service);
            _ctx.SaveChanges();
        }

        public void Update(Service entity)
        {
            var service = _ctx.Tasks.Find(entity.Id);
            _ctx.Entry(service).CurrentValues.SetValues(entity);
            _ctx.SaveChanges();
        }
    }
}
