﻿using System.Collections.Generic;
using ToDoListsManager.DataAccessLayer.Models;

namespace ToDoListsManager.DataAccessLayer.Repositories
{
    public interface IServiceRepository
    {
        Service Get(string id);
        IEnumerable<Service> GetAll();

        string Add(Service entity);
        void Remove(Service entity);
        void Update(Service entity);
    }
}
