﻿using System.Collections.Generic;
using System.Linq;
using ToDoListsManager.DataAccessLayer.Models;

namespace ToDoListsManager.DataAccessLayer.Repositories
{
    internal class TaskRepository : ITaskRepository
    {
        private readonly Contexts.ToDoListContext _ctx;

        public TaskRepository(Contexts.ToDoListContext ctx)
        {
            _ctx = ctx;
        }

        public int Add(Task entity)
        {
            var entry = _ctx.Tasks.Add(entity);
            _ctx.SaveChanges();

            return entry.Entity.Id;
        }

        public Task Get(int id) => _ctx.Tasks.Find(id);
        public Task Get(int localId, string serviceId)
        {
            return _ctx.Tasks
                .Where(t => t.LocalId == localId && t.ServiceId == serviceId).Single();
        }

        public IEnumerable<Task> GetAll() => _ctx.Tasks.AsEnumerable();

        public void Remove(Task entity)
        {
            var task = _ctx.Tasks.Find(entity.Id);
            _ctx.Tasks.Remove(task);
            _ctx.SaveChanges();
        }

        public void Update(Task entity)
        {
            var task = _ctx.Tasks.Find(entity.Id);
            _ctx.Entry(task).CurrentValues.SetValues(entity);
            _ctx.SaveChanges();
        }
    }
}
