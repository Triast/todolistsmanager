﻿using System.Collections.Generic;
using ToDoListsManager.DataAccessLayer.Models;

namespace ToDoListsManager.DataAccessLayer.Repositories
{
    public interface ITaskRepository
    {
        Task Get(int id);
        Task Get(int localId, string serviceId);
        IEnumerable<Task> GetAll();

        int Add(Task entity);
        void Remove(Task entity);
        void Update(Task entity);
    }
}
