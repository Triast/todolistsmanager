﻿CREATE TABLE [dbo].[Tasks]
(
	[Id] INT IDENTITY NOT NULL PRIMARY KEY,
	[LocalId] INT NOT NULL,
	[ServiceId] NVARCHAR(450) NOT NULL,
	[Text] NVARCHAR(MAX) NOT NULL, 
    [IsCompleted] BIT NOT NULL, 
    CONSTRAINT [FK_Tasks_Services] FOREIGN KEY (ServiceId) REFERENCES Services(Id)
)
