## Todo list manager

This is a simple todo list manager project created for demonstration. The application was developed to manage multiple todo list in a single place using message brocker.