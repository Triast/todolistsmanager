# Документация по интеграции с агрегирующим сервисом

Перед началом работы необходимо установить NuGet пакет "RabbitMQ.Client".

## Регистрация сервиса

Регистрация сервиса производится вызова endpoint'а агрегирующего сервиса /api/Services (POST). В body передаётся информация следующего типа:

```csharp
class RegisterMessage
{
	public string Id { get; set; }
	public string Name { get; set; }
	public IEnumerable<TaskDto> Tasks { get; set; }
}
```

Id - идентефикатор сервиса, должен быть уникальным в рамках агрегирующего сервиса. Для удобства лучше всего использовать GUID. Tasks - список всех задач, которые имеются у сервиса. Необходимо для синхронизации с агрегирующим сервисом. Для вызова endpoint'а из кода можно воспользоваться классом HttpClient.

## Отправка сообщений

Отправка сообщений производится передачей в очередь "manager_queue" сообщений следующего типа:

```csharp
class TaskMessage
{
	public string ServiceId { get; set; }
	public string Action { get; set; }
	public string Data { get; set; }
}
```

Где Action - строковое значение "POST" (для добавления), "PATCH" (для обновления) или "DELETE" (для удаления). Data - объект типа TaskDto в JSON формате. Класс TaskDto:

```csharp
class TaskDto
{
	public int Id { get; set; }
	public string Text { get; set; }
	public bool IsCompleted { get; set; }
}
```

Класс для отправки сообщений может выглядеть следующим образом:

```csharp
class MessageSender
{
	private IConfiguration _configuration;

	public MessageSender(IConfiguration configuration)
	{
		_configuration = configuration;
	}

	public void Add(TaskDto task)
	{
		SendMessage("POST", task);
	}

	public void Update(TaskDto task)
	{
		SendMessage("PATCH", task);
	}

	public void Remove(TaskDto task)
	{
		SendMessage("DELETE", task);
	}

	private void SendMessage(string action, TaskDto task)
	{
		var queueName = _configuration.GetValue<string>("ManagerQueue");
		var serviceId = _configuration.GetValue<string>("ServiceId");

		var factory = new ConnectionFactory() { HostName = "localhost" };
		using (var connection = factory.CreateConnection())
		using (var channel = connection.CreateModel())
		{
			channel.QueueDeclare(queue: queueName,
								durable: true,
								exclusive: false,
								autoDelete: false,
								arguments: null);

			var message = new TaskMessage
			{
				ServiceId = serviceId,
				Action = action,
				Data = JsonConvert.SerializeObject(task)
			};

			var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

			channel.BasicPublish(exchange: "",
								routingKey: queueName,
								basicProperties: null,
								body: body);
		}
	}
}
```

queueName - название очереди агрегирующего сервиса. В данном случае "manager_queue".

## Приём сообщений

Приём сообщений производится созданием EventingBasicConsumer'а, который будет получать приходящие от агрегирующего сервиса сообщения и обрабатывать должным образом. Имя очереди, с которой будет работать сервис, должно совпадать с идентефикатором сервиса. Из-за специфичного случая добавления задачи при получении запроса от агрегирующего сервиса на добавление новой задачи, её нужно дополнительно отправить для добавления на агрегирующий сервис. Класс, прослушивающий приходящие сообщения, может выглядеть следующим образом:

```csharp
public class MessageRecievingService : IHostedService
{
	private readonly ITaskService _taskService;

	private readonly IConnection connection;
	private readonly IModel channel;
	private readonly EventingBasicConsumer consumer;

	private readonly string _queueName;

	public MessageRecievingService(ITaskService taskService, IConfiguration configuration, IMessageSender sender)
	{
		_taskService = taskService;
		_queueName = configuration.GetValue<string>("ServiceId");

		var factory = new ConnectionFactory() { HostName = "localhost" };

		connection = factory.CreateConnection();
		channel = connection.CreateModel();

		consumer = new EventingBasicConsumer(channel);
		consumer.Received += (model, ea) =>
		{
			var message = JsonConvert.DeserializeObject<TaskMessage>(Encoding.UTF8.GetString(ea.Body));
			var task = JsonConvert.DeserializeObject<TaskDto>(message.Data);

			switch (message.Action)
			{
				case "POST":
					var id = _taskService.Add(task);
					task.Id = id;
					sender.Add(task);
					break;
				case "PATCH":
					_taskService.Complete(task.Id);
					break;
				case "DELETE":
					_taskService.Remove(task.Id);
					break;
			}
		};
	}

	public Task StartAsync(CancellationToken cancellationToken)
	{
		channel.QueueDeclare(queue: _queueName,
								 durable: true,
								 exclusive: false,
								 autoDelete: false,
								 arguments: null);

		channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);
		channel.BasicConsume(queue: _queueName,
							autoAck: true,
							consumer: consumer);

		return Task.CompletedTask;
	}

	public Task StopAsync(CancellationToken cancellationToken)
	{
		connection.Close();

		return Task.CompletedTask;
	}
}
```

После данный класс нужно зарегестрировать в ConfigureServices:

```csharp
services.AddHostedService<MessageRecievingService>();
```